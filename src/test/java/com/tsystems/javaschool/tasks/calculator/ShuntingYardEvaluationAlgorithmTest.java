package com.tsystems.javaschool.tasks.calculator;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class ShuntingYardEvaluationAlgorithmTest extends TestCase {

    private ShuntingYardEvaluationAlgorithm algorithm = new ShuntingYardEvaluationAlgorithm();

    @Test
    public void testEvaluate() {
        // given
        List<String> input = Arrays.asList("2", "+", "3");
        double expectedResult = 5;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate1() {
        // given
        List<String> input = Arrays.asList("4", "-", "6");
        double expectedResult = -2;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate2() {
        // given
        List<String> input = Arrays.asList("2", "*", "3");
        double expectedResult = 6;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }


    @Test
    public void testEvaluate3() {
        // given
        List<String> input = Arrays.asList("12", "/", "3");
        double expectedResult = 4;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate4() {
        // given
        List<String> input = Arrays.asList("2", "+", "3", "*", "4");
        double expectedResult = 14;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate5() {
        // given
        List<String> input = Arrays.asList("10", "/", "2", "-", "7", "+", "3", "*", "4");
        double expectedResult = 10;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate6() {
        // given
        List<String> input = Arrays.asList("10", "/", "(", "2", "-", "7", "+", "3", ")", "*", "4");
        double expectedResult = -20;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate7() {
        // given
        List<String> input = Arrays.asList("22", "/", "3", "*", "3.0480");
        double expectedResult = 22.352;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testEvaluate8() {
        // given
        List<String> input = Arrays.asList("22", "/", "4", "*", "2.159");
        double expectedResult = 11.8745;

        // run
        double result = algorithm.evaluate(input);

        assertEquals(expectedResult, result);
    }

    @Test(expected = ArithmeticException.class)
    public void testEvaluate9() {
        // given
        List<String> input = Arrays.asList("10", "(", "5", "-", "5", ")");

        // run
        double result = algorithm.evaluate(input);

        // assert (exception)
    }

}