package com.tsystems.javaschool.tasks.calculator;

import junit.framework.TestCase;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;

public class OperatorsAndOperandsParserTest extends TestCase {

    public void testParse() {
        //given
        String input = "2+3";
        List<String> expectedResult = Arrays.asList("2", "+", "3");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse1() {
        //given
        String input = "4-6";
        List<String> expectedResult = Arrays.asList("4", "-", "6");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse2() {
        //given
        String input = "2*3";
        List<String> expectedResult = Arrays.asList("2", "*", "3");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse3() {
        //given
        String input = "12/3";
        List<String> expectedResult = Arrays.asList("12", "/", "3");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse4() {
        //given
        String input = "2+3*4";
        List<String> expectedResult = Arrays.asList("2", "+", "3", "*", "4");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse5() {
        //given
        String input = "10/2-7+3*4";
        List<String> expectedResult = Arrays.asList("10", "/", "2", "-", "7", "+", "3", "*", "4");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse6() {
        //given
        String input = "10/(2-7+3)*4";
        List<String> expectedResult = Arrays.asList("10", "/", "(", "2", "-", "7", "+", "3", ")", "*", "4");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse7() {
        //given
        String input = "22/3*3.0480";
        List<String> expectedResult = Arrays.asList("22", "/", "3", "*", "3.0480");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse8() {
        //given
        String input = "22/4*2.159";
        List<String> expectedResult = Arrays.asList("22", "/", "4", "*", "2.159");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse9() {
        //given
        String input = "22/4*2,159";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse10() {
        //given
        String input = "- 12)1//(";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParser11() {
        //given
        String input = "2 - 6 *99";
        List<String> expectedResult = Arrays.asList("2", "-", "6", "*", "99");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse12() {
        //given
        String input = null;
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse13() {
        //given
        String input = "(12*(5-1)";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }


    public void testParse14() {
        //given
        String input = "";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse15() {
        //given
        String input = "5+41..1-6";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse16() {
        //given
        String input = "5++41-6";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse17() {
        //given
        String input = "5--41-6";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse18() {
        //given
        String input = "5**41-6";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse19() {
        //given
        String input = "5//41-6";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse20() {
        //given
        String input = "5+.(4 - 2)";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    
    public void testParse21() {
        //given
        String input = "5+(/4 - 2)";
        List<String> expectedResult = null;

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    public void testParse22() {
        //given
        String input = "-2+3";
        List<String> expectedResult = Arrays.asList("-2", "+", "3");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }

    public void testParse23() {
        //given
        String input = "1+(-2+3)";
        List<String> expectedResult = Arrays.asList("1", "+", "(", "-2", "+", "3", ")");

        //run
        List<String> result = OperatorsAndOperandsParser.parseOrReturnNull(input);

        //assert
        Assert.assertEquals(expectedResult, result);
    }
}