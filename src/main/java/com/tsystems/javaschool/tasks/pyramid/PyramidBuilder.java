package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param input to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> input) {
        checkInput(input);

        input.sort(Integer::compareTo);
        Iterator<Integer> iter = input.iterator();

        int size = input.size();
        int rows = calcNumberOfRows(size);
        int columns = calcNumberOfColumns(size);

        int[][] pyramid = new int[rows][columns];

        int middleColumnIndex = (2 * rows - 1) / 2;
        for (int i = 0; i < rows; i++) {
            int startColumnIndex = middleColumnIndex - i;
            for (int j = 0; j < i + 1 ; j++) {
                pyramid[i][startColumnIndex + 2 * j] = iter.next();
            }
        }

        return pyramid;
    }

    private void checkInput(List<Integer> input) {
        if (nullCheckFailed(input) || listSizeDoesntMatch(input.size())) {
            throw new CannotBuildPyramidException();
        }
    }

    private boolean nullCheckFailed(List<Integer> input) {
        return input == null || input.contains(null);
    }

    // input list size should be equal to (1 + n) * n / 2,
    // where n is the number of pyramid matrix rows and is the number
    // of list elements in the last row
    private boolean listSizeDoesntMatch(int listSize) {
        double n = (-1 + Math.sqrt(1 + 8 * listSize)) / 2;
        return (int) n != n;
    }

    private int calcNumberOfRows(int listSize) {
        double rows = (-1 + Math.sqrt(1 + 8 * listSize)) / 2;
        return (int) rows;
    }

    private int calcNumberOfColumns(int listSize) {
        int n = calcNumberOfRows(listSize);
        return 2 * n - 1;
    }
}
