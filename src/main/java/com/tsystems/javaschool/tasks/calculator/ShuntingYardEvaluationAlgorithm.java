package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class ShuntingYardEvaluationAlgorithm {
    private Deque<Double> operands = new ArrayDeque<>();
    private Deque<Operator> operators = new ArrayDeque<>();

    public double evaluate(List<String> operatorsAndOperands) {
        for (String in : operatorsAndOperands) {
            if (isOperand(in)) {
                processOperand(in);
            }

            if (isOperator(in)) {
                processOperator(in);
            }

            if (")".equals(in)) {
                processRightBracket();
            }
        }

        processOperatorsStack();

        return operands.getLast();
    }

    private boolean isOperand(String str) {
        String numberRegex = "-?(\\d+|\\d+\\.\\d+)";
        return str.matches(numberRegex);
    }

    private boolean isOperator(String str) {
        char symbol = str.charAt(0);
        return str.length() == 1 && Operator.isOperator(symbol);
    }

    private void processOperand(String in) {
        double number = Double.parseDouble(in);
        operands.add(number);
    }

    private void processOperator(String in) {
        char symbol = in.charAt(0);
        Operator op = Operator.getOperatorBySymbolOrNull(symbol);

        if (operators.isEmpty() || op == Operator.LEFT_BRACKET) {
            operators.offerLast(op);
            return;
        }

        Operator topElement = operators.getLast();
        if (op.precedenceIsLowerThan(topElement)) {
            while (!operators.isEmpty() && op.precedenceIsLowerThan(operators.getLast())) {
                applyLastOperator();
            }
        }

        operators.offerLast(op);
    }

    private void applyLastOperator() {
        Operator operator = operators.pollLast();

        double b = operands.pollLast();
        double a = operands.pollLast();

        if (!operators.isEmpty()) {
            switch(operators.getLast()) {
                case SUBTRACT:
                    operators.pollLast();
                    operators.offerLast(Operator.ADD);
                    a = -a;
                    break;
                case DIVIDE:
                    operators.pollLast();
                    operators.offerLast(Operator.MULTIPLY);
                    a = 1 / a;
                    break;
            }
        }

        double result = operator.apply(a, b);
        operands.offerLast(result);
    }

    private void processRightBracket() {
        while (operators.getLast() != Operator.LEFT_BRACKET) {
            applyLastOperator();
        }

        operators.pollLast();
    }

    private void processOperatorsStack() {
        while(!operators.isEmpty()) {
            applyLastOperator();
        }
    }

}


