package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private final int DECIMAL_PLACES = 4;

    /**
     * Evaluate statement represented as string using Shunting-yard algorithm.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        List<String> operandsAndOperators = OperatorsAndOperandsParser.parseOrReturnNull(statement);
        if (operandsAndOperators == null) {
            return null;
        }

        ShuntingYardEvaluationAlgorithm algorithm = new ShuntingYardEvaluationAlgorithm();

        double result;
        try {
            result = algorithm.evaluate(operandsAndOperators);
        } catch (ArithmeticException e) {
            return null;
        }

        return formatResult(result);
    }

    private String formatResult(double result) {
        double formatted = round(result, DECIMAL_PLACES);
        if ((int) formatted == formatted) {
            return (int) formatted + "";
        }

        return formatted + "";
    }

    private double round(double a, int decimalPlaces) {
        double factor = Math.pow(10, decimalPlaces);
        return Math.round(a * factor) / factor;
    }



}
