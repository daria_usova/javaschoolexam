package com.tsystems.javaschool.tasks.calculator;

import java.util.Arrays;

public enum Operator {
    ADD(1, '+'),
    SUBTRACT(1, '-'),
    MULTIPLY(2, '*'),
    DIVIDE(2, '/'),
    LEFT_BRACKET(0, '(');

    int precedence;
    char symbol;

    Operator(int precedence, char symbol) {
        this.precedence = precedence;
        this.symbol = symbol;
    }

    public boolean precedenceIsLowerThan(Operator operator) {
        return precedence < operator.precedence;
    }

    public double apply(double a, double b) {
        switch (this) {
            case ADD:
                return a + b;
            case SUBTRACT:
                return a - b;
            case MULTIPLY:
                return a * b;
            case DIVIDE:
                if (b == 0.0) {
                    throw new ArithmeticException("Dividing by zero exception");
                }
                return a / b;
            default:
                return 0;
        }
    }

    public static Operator getOperatorBySymbolOrNull(char symbol) {
        return Arrays.stream(Operator.values())
                .filter(op -> op.symbol == symbol)
                .findAny()
                .orElse(null);
    }

    public static boolean isOperator(char symbol) {
        return getOperatorBySymbolOrNull(symbol) != null;
    }
}
