package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class OperatorsAndOperandsParser {

    private static final String DIGITS = "0123456789";
    private static final String SEPARATOR = ".";
    private static final String OPERATORS = "+-/*";
    private static final String PARENTHESES = "()";
    
    private static final String ALLOWED_SYMBOLS = DIGITS + SEPARATOR + OPERATORS + PARENTHESES;

    private static final String OPERATORS_REGEX = "\\+\\-\\/\\*";
    private static final String SEPARATOR_REGEX = "\\.";

    public static List<String> parseOrReturnNull(String input) {
        if (input == null || "".equals(input)) {
            return null;
        }

        String inputWoSpaces = input.replaceAll("\\s+", "");

        if (inputIsIncorrect(inputWoSpaces)) {
            return null;
        }

        return parse(inputWoSpaces);
    }

    private static boolean inputIsIncorrect(String input) {
        return containsNotAllowedSymbols(input) ||
                containsNotClosedParenthesis(input) ||
                containsNotAllowedSequenceOfSymbols(input);
    }

    private static boolean containsNotAllowedSymbols(String input) {
        return input.chars()
                .mapToObj(ch -> (char)ch)
                .anyMatch(ch -> !ALLOWED_SYMBOLS.contains(ch + ""));
    }

    private static boolean containsNotClosedParenthesis(String input) {
        Deque<Character> parenthesisStack = new ArrayDeque<>();

        for (char ch : input.toCharArray()) {
            switch (ch) {
                case '(':
                    parenthesisStack.offerLast(ch);
                    break;
                case ')':
                    if (parenthesisStack.isEmpty()) {
                        return true;
                    }

                    parenthesisStack.pollLast();
                    break;
            }
        }

        boolean allParenthesisAreClosed = parenthesisStack.isEmpty();
        return !allParenthesisAreClosed;

    }

    private static boolean containsNotAllowedSequenceOfSymbols(String input) {
        return containsOperatorsAfterOperators(input) ||                // ++
                containsOperatorBeforeParenthesis(input) ||             // +)
                containsOperatorAfterParenthesis(input) ||              // (+, but (- is allowed
                containsSeparatorAfterOrBeforeOperator(input) ||        // .+ or +.
                containsSeparatorAfterOrBeforeParentheses(input) ||     // (. or .(, ); ). or .)
                containsSeparatorAfterSeparator(input);                 // ..
    }

    private static boolean containsOperatorsAfterOperators(String input) {
        String regex = String.format(".*[%s][%s]+.*", OPERATORS_REGEX, OPERATORS_REGEX);
        return input.matches(regex);
    }

    private static boolean containsOperatorBeforeParenthesis(String input) {
        String regex = String.format(".*[%s]\\).*", OPERATORS_REGEX);
        return input.matches(regex);
    }

    private static boolean containsOperatorAfterParenthesis(String input) {
        String operatorsWoMinus = OPERATORS_REGEX.replace("-", "");
        String regex = String.format(".*\\([%s].*", operatorsWoMinus);
        return input.matches(regex);
    }

    private static boolean containsSeparatorAfterOrBeforeOperator(String input) {
        String regex = String.format(".*([%s][%s]|[%s][%s]).*", SEPARATOR_REGEX, OPERATORS_REGEX
                                                              , OPERATORS_REGEX, SEPARATOR_REGEX);
        return input.matches(regex);
    }

    private static boolean containsSeparatorAfterOrBeforeParentheses(String input) {
        String parentheses = "\\(\\)";
        String regex = String.format(".*([%s][%s]|[%s][%s]).*", SEPARATOR_REGEX, parentheses
                                                              ,parentheses, SEPARATOR_REGEX);
        return input.matches(regex);
    }

    private static boolean containsSeparatorAfterSeparator(String input) {
        String regex = String.format(".*[%s][%s]+.*", SEPARATOR_REGEX, SEPARATOR_REGEX);
        return input.matches(regex);
    }

    private static List<String> parse(String input) {
        List<String> output = new ArrayList<>();

        String number = "";
        for (char ch : input.toCharArray()) {
            if (isDigit(ch) || isUnaryMinus(ch, number)) {
                number += ch;
            } else {
                addIfNotEmpty(number, output);
                output.add(ch + "");

                number = "";
            }
        }

        addIfNotEmpty(number, output);

        return output;
    }

    private static boolean isDigit(char ch) {
        return (ch + "").matches("[\\d\\.]");
    }

    private static boolean isUnaryMinus(char ch, String number) {
        return ch == '-' && "".equals(number);
    }

    private static void addIfNotEmpty(String str, List<String> list) {
        if (!"".equals(str)) {
            list.add(str);
        }
    }


}
