package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param subsequence first sequence
     * @param sequence second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List subsequence, List sequence) {
        if (subsequence == null || sequence == null) {
            throw new IllegalArgumentException();
        }

        Iterator iterSeq = sequence.iterator();
        for (Object subObj : subsequence) {
            while (iterSeq.hasNext() && !Objects.equals(subObj, iterSeq.next())) {}

            if (!iterSeq.hasNext()) {
                return false;
            }
        }

        return true;
    }

}
